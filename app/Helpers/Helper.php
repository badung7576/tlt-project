<?php

namespace App\Helpers;

use Carbon\Carbon;

if (!function_exists('calculate_age')) {

    function calculate_age($birthday)
    {
        $age = Carbon::parse($birthday)->age;
        return $age;
    }
}
if (!function_exists('format_date_dmy')) {

    function format_date_dmy($date)
    {
        $age = Carbon::parse($date)->format('d/m/Y');
        return $age;
    }
}
