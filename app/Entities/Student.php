<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $fillable = ['name', 'birthday', 'hobby','c_username', 'c_password', 'school', 'parent_id', 'status'];

    public function parent() {
        return $this->belongsTo('App\Entities\Parents', 'parents_id', 'id');
    }
}
