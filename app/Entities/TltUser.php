<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TltUser extends Model
{
    protected $table = 'tlt_users';
    protected $fillable = ['fname', 'lname', 'username', 'password', 'role', 'phonenumber'];
}
