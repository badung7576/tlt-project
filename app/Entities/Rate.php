<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';
    protected $fillable = ['student_id', 'comment', 'user_id', 'rate_time'];
}
