<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Rollup extends Model
{
    protected $table = 'rollups';
    protected $fillable = ['date', 'student_course_id', 'check', 'homework', 'comment'];

    public function lesson() {
        return $this->belongsTo('App\Entities\Lesson', 'lesson_id', 'id');
    }
}
