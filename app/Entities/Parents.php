<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{
    protected $table = 'parents';
    protected $fillable = ['name', 'mail', 'phonenumber', 'adrress'];
}
