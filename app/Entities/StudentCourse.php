<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class StudentCourse extends Model
{
    protected $table = 'student_courses';
    protected $fillable = [];

    public function student() {
        return $this->belongsTo('App\Entities\Student', 'student_id', 'id');
    }

    public function area() {
        return $this->belongsTo('App\Entities\Area', 'area_id', 'id');
    }

    public function course() {
        return $this->belongsTo('App\Entities\Course', 'course_id', 'id');
    }
}
