<?php

namespace App\Http\Controllers;

use App\Services\StudentService;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    protected $studentService;
    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }

    public function index(Request $request) {
        $areaId = $request->has('area_id') ? $request->get('area_id') : 0;
        $studentInfs = $this->studentService->getStudents($areaId);
        $areas = $this->studentService->getAreas();
        return view('students.students_list', compact('studentInfs', 'areas', 'areaId'));
    }

    public function getEdit(Request $request) {
        $studentId = $request->get('id');
        $student = $this->studentService->getStudent($studentId);
        $form = view('students._editModal', compact('student'))->render();
        return response()->json(compact('form'));
    }

    public function getInf(Request $request) {
        $studentId = $request->get('id');
        $studentInf = $this->studentService->getStundentInf($studentId);
        $rollupInfs = $this->studentService->getRollupInf($studentId);
        $table = view('students._showModal', compact('studentInf', 'rollupInfs'))->render();
        return response()->json(compact('table'));
    }

    public function updateEdit(Request $request) {
        $this->studentService->updateEdit($request, $request->get('id'));
    }

    public function add() {
        return view('students.students_add');
    }

    public function postAdd() {

    }

    public function comment() {
        return view('students.students_comment');
    }

    public function delete($id) {
        $this->studentService->deleteStudent($id);
        return redirect()->route('admin.students.index');
    }

}
