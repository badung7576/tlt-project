<?php

namespace App\Http\Controllers;

use App\Services\DashBoardService;


class AdminController extends Controller
{
    protected $dashboardService;
    public  function  __construct(DashBoardService $dashBoardService)
    {
        $this->dashboardService = $dashBoardService;
    }
    public function index() {
        $teamMember = $this->dashboardService->countTeamMembers();
        $students = $this->dashboardService->countStudents();
        $areas = $this->dashboardService->countAreas();
        return view('dashboard.dashboard', compact('teamMember', 'students', 'areas'));
    }
}
