<?php
namespace App\Services;


use App\Entities\Area;
use App\Entities\Student;
use App\Entities\TltUser;

class DashBoardService
{
    public function countTeamMembers() {
        return TltUser::all()->count();
    }
    public function countStudents() {
        return Student::all()->count();
    }
    public function countAreas() {
        return Area::all()->count();
    }
}
