<?php
namespace App\Services;

use App\Entities\Area;
use App\Entities\Rollup;
use App\Entities\Student;
use App\Entities\StudentCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StudentService
{
    const LEARNING_STATE = 1;
    public function getStudents($areaId) {
        if($areaId != 0 ) {
            $studentCourses = StudentCourse::with('student', 'area', 'course')
                ->where(['status' => self::LEARNING_STATE, 'area_id' => $areaId])->get();
        } else {
            $studentCourses = StudentCourse::with('student', 'area', 'course')
                ->where(['status' => self::LEARNING_STATE])->get();
        }
        $datas = [];
        foreach ($studentCourses as $sc) {
            $obj = [
                'id' => $sc->student_id,
                'name' => $sc->student->fullname,
                'birthday' => $sc->student->birthday,
                'course_name' => $sc->course->name,
                'num_session' => $sc->num_session,
                'payment_status' => $sc->payment_status,
                'vote' => $sc->student->vote
            ];
            $datas[] = $obj;
        }
        return $datas;
    }

    public function getStundentInf($id) {
        $studentCourse = StudentCourse::with('student', 'area', 'course')->where(['student_id' => $id, 'status' => self::LEARNING_STATE])->first();
        $data = [
            'id' => $studentCourse->student_id,
            'name' => $studentCourse->student->fullname,
            'birthday' => $studentCourse->student->birthday,
            'course_name' => $studentCourse->course->name,
            'num_session' => $studentCourse->num_session,
            'payment_status' => $studentCourse->payment_status,
            'vote' => $studentCourse->student->vote
        ];
        return $data;
    }

    public function getRollupInf($id) {
        $studentCourse = StudentCourse::where(['student_id' => $id, 'status' => self::LEARNING_STATE])->first();
        $rollups = Rollup::with('lesson')->where(['student_course_id' => $studentCourse->id, 'check' => self::LEARNING_STATE])->get();
        return $rollups;
    }

    public function getAreas() {
        return Area::all();
    }

    public function deleteStudent($id) {
        Student::find($id)->delete();
        StudentCourse::where('student_id', $id)->delete();
    }

    public function getStudent($id) {
        return Student::find($id);
    }

    public function updateEdit(Request $request, $id) {
        try {
            Carbon::parse($request->get('birthday'));
        } catch (\Exception $e) {
            return;
        }
        $data = [
            'fullname' => $request->get('fullname'),
            'birthday' => Carbon::parse($request->get('birthday')),
            'hobby' => $request->get('hobby'),
            'c_username' => $request->get('c_username'),
            'c_password' => $request->get('c_password'),
            'status' => $request->get('status')
        ];
        Student::where('id', $id)->update($data);
    }
}
