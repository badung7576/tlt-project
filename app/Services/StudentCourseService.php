<?php
namespace App\Services;

use App\Entities\StudentCourse;
class StudentCourseService
{
    public function getStudents($conditions) {
        $students = StudentCourse::where($conditions)->get();
        return $students;
    }
}
