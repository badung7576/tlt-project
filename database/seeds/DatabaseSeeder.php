<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AreaTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(ParentTableSeeder::class);
        $this->call(RateTableSeeder::class);
        $this->call(RollupTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(StudentCourseTableSeeder::class);
        $this->call(TLTUserTableSeeder::class);
    }
}
