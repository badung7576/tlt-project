<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'LoginController@login')->name('admin.login');


Route::group(['prefix' => 'admin'], function(){
    // dashboard
    Route::get('dashboard', 'AdminController@index')->name('admin.index');

    //students
    Route::get('students-list', 'StudentController@index')->name('admin.students.index');
    Route::get('students-add', 'StudentController@add')->name('admin.students.add');
    Route::post('students-add', 'StudentController@addStudent')->name('admin.students.add');
    Route::get('students-comment', 'StudentController@comment')->name('admin.students.comment');
    Route::get('students-delete/{id}', 'StudentController@delete')->name('admin.students.delete');

    Route::get('students-get-edit', 'StudentController@getEdit')->name('admin.students.getEdit');
    Route::get('students-get-inf', 'StudentController@getInf')->name('admin.students.getInf');
    Route::post('students-update', 'StudentController@updateEdit')->name('admin.students.updateEdit');


    // Rollup
    Route::get('rollup-list', 'RollupController@index')->name('admin.rollup.index');
    Route::get('rollup-add', 'RollupController@add')->name('admin.rollup.add');

    // Parents
    Route::get('parents-list', 'RollupController@index1')->name('admin.parents.index');
});



