@extends('layout.master')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><strong>Dashboard</strong></h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                Dashboard
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="widget style1 blue-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <span> Students </span>
                        <h2>{{ $students }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-map-marker fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <span> Address </span>
                        <h2> {{ $areas }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="widget style1 red-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-user-o fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <span> Team </span>
                        <h2>{{ $teamMember }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function startTime() {
        let today = new Date();
        let h = today.getHours();
        let m = today.getMinutes();
        let s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        $('#realtime').text( h + ":" + m + ":" + s);
        let t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
    $( document ).ready(function(){
        startTime();
    });
</script>
@stop


