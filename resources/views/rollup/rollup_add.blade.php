@extends('layout.master')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thông tin điểm danh</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Thông tin điểm danh</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Bách Khoa</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label"><strong>Khu vực</strong></label>
                            <div class="col-sm-2">
                                <select class="form-control b-r-sm" name="account">
                                    <option>Bách Khoa</option>
                                    <option>Nguyễn Chí Thanh</option>
                                </select>
                            </div>
                            <label class="col-sm-1 col-form-label"><strong>Thời gian</strong></label>
                            <div class="col-sm-2">
                                <select class="form-control b-r-sm" name="account">
                                    <option>Sáng</option>
                                    <option>Chiều</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Thông tin học</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th width="20%">Họ tên</th>
                                <th width="15%">Ngày học</th>
                                <th width="15%">Học/Nghỉ</th>
                                <th width="25%">Nội dung</th>
                                <th width="25%">BTVN</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Nguyễn Văn A</td>
                                <td><input type="text" class="form-control datepicker" name="" style="width: 120px"></td>
                                <td>
                                    <label> <input type="radio" value="1" name="status"> <i></i> Có </label>
                                    <label> <input type="radio" value="1" name="status"> <i></i> Không </label>
                                </td>
                                <td>
                                    <select class="form-control b-r-sm" name="account">
                                        <option>Sáng</option>
                                        <option>Chiều</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="" >
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Nguyễn Văn A</td>
                                <td><input type="text" class="form-control datepicker" name="" style="width: 120px"></td>
                                <td>
                                    <label> <input type="radio" value="1" name="status"> <i></i> Có </label>
                                    <label> <input type="radio" value="1" name="status"> <i></i> Không </label>
                                </td>
                                <td>
                                    <select class="form-control b-r-sm" name="account">
                                        <option>Sáng</option>
                                        <option>Chiều</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="" >
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Nguyễn Văn A</td>
                                <td><input type="text" class="form-control datepicker" name="" style="width: 120px"></td>
                                <td>
                                    <label> <input type="radio" value="1" name="status"> <i></i> Có </label>
                                    <label> <input type="radio" value="1" name="status"> <i></i> Không </label>
                                </td>
                                <td>
                                    <select class="form-control b-r-sm" name="account">
                                        <option>Sáng</option>
                                        <option>Chiều</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="" >
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                dateFormat: "yy/mm/dd",
            });
        });
    </script>
@stop
