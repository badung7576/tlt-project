@extends('layout.master')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thông tin điểm danh</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Thông tin điểm danh</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Bách Khoa</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label"><strong>Khu vực</strong></label>
                            <div class="col-sm-2">
                                <select class="form-control b-r-sm" name="account">
                                    <option>Bách Khoa</option>
                                    <option>Nguyễn Chí Thanh</option>
                                </select>
                            </div>
                            <label class="col-sm-1 col-form-label"><strong>Thời gian</strong></label>
                            <div class="col-sm-2">
                                <select class="form-control b-r-sm" name="account">
                                    <option>Sáng</option>
                                    <option>Chiều</option>
                                </select>
                            </div>
                            <label class="col-sm-1 col-form-label"><strong> Từ ngày </strong></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control datepicker" name="birthday" >
                            </div>
                            <label class="col-sm-1 col-form-label"><strong> Đến ngày </strong></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control datepicker" name="birthday" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Thông tin học</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover text-center" >
                                <thead>
                                <tr>
                                    <th>Thời gian</th>
                                    <th>Học sinh 1</th>
                                    <th>Học sinh 2</th>
                                    <th>Học sinh 3</th>
                                    <th>Học sinh 4</th>
                                    <th>Học sinh 5</th>
                                    <th>Học sinh 6</th>
                                    <th>Học sinh 7</th>
                                    <th>Học sinh 8</th>
                                    <th>Học sinh 9</th>
                                    <th>Học sinh 10</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                <tr>
                                    <td>2017-10-08</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                </tr>
                                <tr>
                                    <td>2017-10-08</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                </tr>
                                <tr>
                                    <td>2017-10-08</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                </tr>
                                <tr>
                                    <td>2017-10-08</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                </tr>
                                <tr>
                                    <td>2017-10-08</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                </tr>
                                <tr>
                                    <td>2017-10-08</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>O</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>X</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                dateFormat: "yy/mm/dd",
            });
        });
    </script>
@stop
