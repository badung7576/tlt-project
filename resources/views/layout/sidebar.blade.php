<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="{{ asset('img/tlt_avatar.png') }}" width="40" height="40"/>
                    <a href="#">
                        <span class="m-t-xs font-bold">Bá Dũng</span>
                    </a>
                </div>
                <div class="logo-element"> TLT </div>
            </li>
            <li>
                <a href="{{ route('admin.index') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-child"></i> <span class="nav-label">Học sinh</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('admin.students.index') }}">Danh sách học sinh</a></li>
                    <li><a href="{{ route('admin.students.add') }}">Thêm</a></li>
                    <li><a href="{{ route('admin.students.comment') }}">Nhận xét</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-group"></i> <span class="nav-label">Phụ huynh</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('admin.parents.index') }}">Danh sách phụ huynh</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Điểm danh</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('admin.rollup.index') }}">Danh sách điểm danh</a></li>
                    <li><a href="{{ route('admin.rollup.add') }}">Điểm danh học sinh</a></li>
                    <li><a href="#">Điểm danh dạy</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-location-arrow"></i> <span class="nav-label">Khóa học</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#">Nội dung</a></li>
                    <li><a href="#">Thêm bài học</a></li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
