@extends('layout.master')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><strong>Dashboard</strong></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    Dashboard
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Khu vực : Bách Khoa</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Tên</th>
                                <th>Số buổi</th>
                                <th>Khóa đang học</th>
                                <th>Nhận xét</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Khu vực : Nguyễn Chí Thanh</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Tên</th>
                                <th>Số buổi</th>
                                <th>Khóa đang học</th>
                                <th>Nhận xét</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                                <td>Samantha</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
