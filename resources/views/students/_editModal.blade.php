<form method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Họ và tên</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="fullname" value="{{ $student->fullname }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Ngày sinh</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="datepicker" name="birthday" value="{{ $student->birthday }}">
        </div>
    </div>

    <div class="form-group  row">
        <label class="col-sm-2 col-form-label">Sở thích</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="hobby" value=" {{ $student->hobby }} ">
        </div>
    </div>

    <div class="form-group  row">
        <label class="col-sm-2 col-form-label">Trường</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="school" value="{{ $student->school }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Tài khoản khóa học</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="c_username" value="{{ $student->c_username }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Mật khẩu khóa học</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="c_password" value="{{ $student->c_password }}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Đang học</label>
        <div class="col-sm-10">
            <div class="i-checks">
                <label> <input type="radio" value="1" name="status" @if( $student->status === 1) checked @endif> <i></i> Có </label>
                <label> <input type="radio" value="0" name="status" @if( $student->status === 0) checked @endif> <i></i> Không </label>
            </div>
        </div>
    </div>
</form>
<script>
    $(document).ready(function() {
        $('#datepicker').datepicker({
            dateFormat: "yy/mm/dd",
        });
    });
</script>
