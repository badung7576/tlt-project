@extends('layout.master')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><strong>Học sinh</strong></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    Học sinh
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <label class="col-form-label"></label>
            <div class="">
                <select class="form-control b-r-sm" id="areas">
                    <option value="0" @if( $areaId == 0)  selected @endif>Tất cả các khu vực</option>
                    @foreach($areas as $area)
                        <option value="{{$area->id}}" @if( $areaId == $area->id)  selected @endif>{{ $area->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                        <tr>
                            <th data-toggle="true">ID</th>
                            <th data-toggle="true">Tên</th>
                            <th data-hide="phone">Tuổi</th>
                            <th data-hide="phone">Khóa đang học</th>
                            <th data-hide="phone,tablet" >Timeline</th>
                            <th data-hide="phone,tablet">Học phí</th>
                            <th data-hide="phone">Đánh giá</th>
                            <th class="text-right" data-sort-ignore="true">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($studentInfs as $index => $student)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $student['name'] }}</td>
                                <td>{{ \App\Helpers\calculate_age($student['birthday']) }}</td>
                                <td>{{ $student['course_name'] }}</td>
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar" style="width: {{ $student['num_session']*10 }}%" role="progressbar">
                                            @if( $student['num_session'] !== 0)
                                                {{$student['num_session']}}
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                @if($student['payment_status'] === 1)
                                    <td><span class="badge badge-primary"> Đã đóng</span></td>
                                @else
                                    <td><span class="badge badge-danger"> Chưa đóng</span></td>
                                @endif
                                <td>
                                    @for($i = 1; $i <= $student['vote']; $i++)
                                        <i class="fa fa-star" style="color: orangered"></i>
                                    @endfor
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <a class="btn-white btn btn-xs" data-toggle="modal" data-target="#ShowModal" onclick="show({{$student['id']}})">Show</a>
                                        <a class="btn-white btn btn-xs" data-toggle="modal" data-target="#EditModal" onclick="edit({{$student['id']}})">Edit</a>
                                        <a class="btn-white btn btn-xs" href="{{ route('admin.students.delete', $student['id']) }}">Delete</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="EditModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sửa thông tin học sinh</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="_formEdit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submitEdit">Update</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="ShowModal">
        <div class="modal-dialog modal-lg" role="document">
            <div id="_showInf"></div>
        </div>
    </div>
    <script>
        function edit(id) {
            $.ajax({
                url: "{{ URL::route('admin.students.getEdit') }}",
                type: 'get',
                data: {
                    id : id
                },
                success: function (data) {
                    $('#_formEdit').html(data.form);
                    $('#submitEdit').attr('onclick', 'update(' + id +')');
                }
            })
        }
        function show(id) {
            $.ajax({
                url: "{{ URL::route('admin.students.getInf') }}",
                type: 'get',
                data: {
                    id : id
                },
                success: function (data) {
                        $('#_showInf').html(data.table);
                }
            })
        }

        function update(id) {
            $.ajax({
                url: "{{ URL::route('admin.students.updateEdit') }}",
                type: 'post',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id : id,
                    fullname: $('#EditModal input[name=fullname]').val(),
                    birthday: $('#EditModal input[name=birthday]').val(),
                    hobby: $('#EditModal input[name=hobby]').val(),
                    school: $('#EditModal input[name=school]').val(),
                    c_username: $('#EditModal input[name=c_username]').val(),
                    c_password: $('#EditModal input[name=c_password]').val(),
                    status: $('#EditModal input[name=status]').val(),
                },
                success: function (data) {
                    $('#EditModal').modal('hide');
                    window.location.reload();
                }
            })
        }
        $(document).ready(function() {
            $('.footable').footable();

            $('#areas').on('change', function() {
                window.location = "{{ route('admin.students.index') }}?area_id=" + $('#areas').val();
            });
        });

    </script>
    </div>
@stop
