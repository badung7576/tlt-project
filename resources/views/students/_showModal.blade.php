<div class="modal-content">
    <div class="modal-header">
        <h3 class="modal-title">Thông tin học sinh</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body bg-warning ">
        <div class="widget p-xl" >
            <h2>
                {{ $studentInf['name'] }}
            </h2>
            <ul class="list-unstyled m-t-md" style="font-size: 16px">
                <li>
                    <label style="font-weight: bold">Tuổi:</label> {{ \App\Helpers\calculate_age($studentInf['birthday']) }}
                </li>
                <li>
                    <label style="font-weight: bold">Khóa học tham gia:</label> {{ $studentInf['course_name'] }}
                </li>
                <li>
                    <label style="font-weight: bold">Timeline:</label>
                    <div class="progress">
                        <div class="progress-bar bg-primary" style="width: {{ $studentInf['num_session']*10 }}%" role="progressbar">
                            @if( $studentInf['num_session'] !== 0)
                                {{ $studentInf['num_session'] }}
                            @endif
                        </div>
                    </div>
                </li>
                <li>
                    <label style="font-weight: bold">Học phí:</label>
                    <div>
                    @if($studentInf['payment_status'] === 1)
                        <span class="badge badge-warning"> Đã đóng</span>
                    @else
                        <span class="badge badge-danger"> Chưa đóng</span>
                    @endif
                    </div>
                </li>
                <li>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ngày học</th>
                            <th>Bài học</th>
                            <th>Bài tập</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $rollupInfs as $index => $rollup)
                            <tr>
                                <td>{{ ++$index }}</td>
                                <td>{{ \App\Helpers\format_date_dmy($rollup->date) }}</td>
                                <td>{{ $rollup->lesson->name }}</td>
                                @if( !empty($rollup->homework))
                                    <td><a href="{{ $rollup->homework }}"> Link bài tập </a></td>
                                @else
                                    <td>Chưa làm btvn</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </li>
            </ul>

        </div>
    </div>
</div>
