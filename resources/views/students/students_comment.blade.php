@extends('layout.master')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><strong>Nhận xét học sinh</strong></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    Nhận xét học sinh
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Nhận xét học sinh</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="get">
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Học sinh</label>
                                <div class="col-sm-10">
                                    <select class="form-control m-b" name="account">
                                        <option>Nguyễn Văn A</option>
                                        <option>Nguyễn Văn B</option>
                                        <option>Đặng Văn C</option>
                                        <option>Nguyễn Thị T</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Nhận xét</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="stars col-sm-12">
                                    <input class="star star-1" id="star-1" type="radio" name="star" value="5"/>
                                    <label class="star star-1" for="star-1"></label>
                                    <input class="star star-2" id="star-2" type="radio" name="star" value="4"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-4" id="star-4" type="radio" name="star" value="2"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-5" id="star-5" type="radio" name="star" value="1"/>
                                    <label class="star star-5" for="star-5"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-primary btn-sm" type="submit">Xác nhận</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
