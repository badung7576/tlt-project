@extends('layout.master')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><strong>Phụ huynh</strong></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    Phụ huynh
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">

                        <table class="footable table table-stripped toggle-arrow-tiny">
                            <thead>
                            <tr>
                                <th data-toggle="true">ID</th>
                                <th data-toggle="true">Tên</th>
                                <th data-hide="phone">Tên con</th>
                                <th data-toggle="true">SĐT</th>
                                <th  data-hide="phone">Email</th>
                                <th data-hide="phone">Địa chỉ</th>
                                <th data-hide="phone" >Ghi chú</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Nguyễn Thị A</td>
                                <td>Nguyễn Văn B</td>
                                <td>19006600</td>
                                <td>nguyethiA@gmail.com</td>
                                <td>Hồ Tây sfkajfajslfjalfjaljflasfjlasjfljfldấdtsghshggs</td>
                                <td>Giảm 20% học phí</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Nguyễn Thị A</td>
                                <td>Nguyễn Văn B</td>
                                <td>19006600</td>
                                <td>nguyethiA@gmail.com</td>
                                <td>Hồ Tây</td>
                                <td>Giảm 20% học phí</td>
                            </tr>
                             <tr>
                                <td>1</td>
                                <td>Nguyễn Thị A</td>
                                <td>Nguyễn Văn B</td>
                                <td>19006600</td>
                                <td>nguyethiA@gmail.com</td>
                                <td>Hồ Tây</td>
                                <td>Giảm 20% học phí</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.footable').footable();
        });
    </script>
@stop
